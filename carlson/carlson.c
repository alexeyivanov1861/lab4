#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TREATS_SIZE	100
#define EX_SIZE		100

void fill(int *t, int *e) {
	int i;
	for (i = 0; i < TREATS_SIZE; i++)
		t[i] = rand() % 5;
	for (i = 0; i < EX_SIZE; i++)
		e[i] = -(rand() % 5);
}

int main(void) {
	int carlson = 60;
	int *treats = malloc(sizeof(int)*TREATS_SIZE);
	int *exers = malloc(sizeof(int)*EX_SIZE);
	srand(time(NULL));
	fill(treats, exers);
	int i = 0, j = 0;
	while (1) {
		if (i == TREATS_SIZE) {
			printf("Carlson ate all jam\n");
			break;
		}
		if (j == EX_SIZE) {
			printf("Carlson beat all thieves\n");
			break;
		}
		if (carlson >= 100) {
			printf("Carlson become too fat\n");
			break;
		}
		if (carlson <= 20) {
			printf("Carlson died\n");
			break;
		}
		int r = rand() % 2;
		if (r) {
			carlson += treats[i];
			++i;
		} else {
			carlson += exers[j];
			j++;
		}
	}
	free(treats);
	free(exers);
	return 0;
}
