#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#define IS_DIGIT(c)	('0' <= (c) && (c) <= '9')

void check(const char* file, const char* planet) {
	FILE* f = fopen(file, "r");
	static char buf[1024];
	fgets(buf, 1024, f);
	if (strstr(buf, planet))
		puts(file);
}

void search(const char* planet) {
	DIR* d = opendir(".");
	struct dirent* ent = readdir(d);
	struct stat fileinfo;
	while (ent != NULL) {
		if (lstat(ent->d_name, &fileinfo)) {
			printf("Error\n");
			exit(__LINE__);
		}
		if (S_ISREG(fileinfo.st_mode))
			check(ent->d_name, planet);
		ent = readdir(d);
	}
	closedir(d);
}

int main(void) {
	static char planet[1001];
	scanf("%1000s", planet);
	search(planet);
	return 0;
}
